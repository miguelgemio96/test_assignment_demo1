# Jose Miguel Gemio Quispe
## Python Training Assignment - DEMO1

There are unit tests defined in this folder **path todev_demos/demo2/tests**

The unit tests are testing "core" functions, you are free to add more unit tests
, any other function that helps to implement "core" functions are not required to have
 unit tests unless required

Your work consists on executing the following **TODO** items


###### To check the pep8 errors, run the following command:
  ```bash
  $ pycodestyle --show-source --show-pep8 --max-line-length=100 ./tests/. ./src/.  
  ```

###### To Run the tests type th following:
  ```bash
  $ pytest 
  # Or you cand run the tests separated:
  $ pytest ./src/.
  $ pytest ./tests/.
  ```
