from src.dates_helper import get_dates_in_interval, get_default_date_data


def test_one_date():
    start_date = '9/12/2022'
    end_date = '9/12/2022'
    dates = get_dates_in_interval(start_date, end_date)
    assert '9/12/2022' in dates


def test_dates_interval():
    start_date = '9/12/2022'
    end_date = '9/15/2022'
    dates = get_dates_in_interval(start_date, end_date)
    assert '9/12/2022' in dates
    assert '9/13/2022' in dates
    assert '9/14/2022' in dates
    assert '9/15/2022' in dates


def test_dates_none_return():
    start_date = '9/15/2022'
    end_date = '9/10/2022'
    dates = get_dates_in_interval(start_date, end_date)
    assert dates is None


def test_dates_none_inputs():
    start_date = None
    end_date = None
    dates = get_dates_in_interval(start_date, end_date)
    assert dates is None


def test_dates_invalid_format():
    start_date = '9/45/2022'
    end_date = '9/10/2022'
    dates = get_dates_in_interval(start_date, end_date)
    assert dates is None


def test_dates_default_one_date():
    start_date = '9/12/2022'
    end_date = '9/12/2022'
    default_value = -99999
    dates = get_default_date_data(start_date, end_date, default_value)
    assert {'date': '9/12/2022', 'participants': -99999} in dates


def test_dates_default_none():
    start_date = '9/12/2022'
    end_date = '9/15/2022'
    default_value = None
    dates = get_default_date_data(start_date, end_date, default_value)
    assert {'date': '9/12/2022', 'participants': None} in dates
    assert {'date': '9/13/2022', 'participants': None} in dates
    assert {'date': '9/14/2022', 'participants': None} in dates
    assert {'date': '9/15/2022', 'participants': None} in dates


def test_dates_default_interval():
    start_date = '9/12/2022'
    end_date = '9/15/2022'
    default_value = -99999
    dates = get_default_date_data(start_date, end_date, default_value)
    assert {'date': '9/12/2022', 'participants': -99999} in dates
    assert {'date': '9/13/2022', 'participants': -99999} in dates
    assert {'date': '9/14/2022', 'participants': -99999} in dates
    assert {'date': '9/15/2022', 'participants': -99999} in dates


def test_dates_default_invalid_date():
    start_date = '9/15/2022'
    end_date = '9/10/2022'
    default_value = -99999
    dates = get_default_date_data(start_date, end_date, default_value)
    assert len(dates) == 0


def test_dates_default_nones():
    start_date = None
    end_date = None
    default_value = -99999
    dates = get_default_date_data(start_date, end_date, default_value)
    assert len(dates) == 0


def test_dates_default_string():
    start_date = '9/12/2022'
    end_date = '9/15/2022'
    default_value = "0h 0m"
    dates = get_default_date_data(start_date, end_date, default_value)
    assert {'date': '9/12/2022', 'participants': '0h 0m'} in dates
    assert {'date': '9/13/2022', 'participants': '0h 0m'} in dates
    assert {'date': '9/14/2022', 'participants': '0h 0m'} in dates
    assert {'date': '9/15/2022', 'participants': '0h 0m'} in dates


def test_dates_default_invalid_date_format():
    start_date = '49/15/2022'
    end_date = '9/10/2022'
    default_value = -99999
    dates = get_default_date_data(start_date, end_date, default_value)
    assert len(dates) == 0
