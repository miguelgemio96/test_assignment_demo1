from datetime import datetime, timedelta

DATE_FORMAT = "%m/%d/%Y"


def str_to_date(str_date):
    return datetime.strptime(str_date, DATE_FORMAT).date()


def date_to_str(date):
    str_date = date.strftime(DATE_FORMAT)
    return str_date[1:] if str_date[0] == '0' else str_date[:]


def validate_input_date_format(start_date, end_date):
    try:
        start_date, end_date = str_to_date(start_date), str_to_date(end_date)
        return start_date, end_date
    except Exception as e:
        print(e)
        return None, None


def get_dates_in_interval(start_date, end_date):
    start_date, end_date = validate_input_date_format(start_date, end_date)
    if start_date and end_date and start_date <= end_date:
        days_range, output = abs((end_date - start_date).days) + 1, []
        for _ in range(days_range):
            str_date = date_to_str(start_date)
            output.append(str_date)
            start_date += timedelta(days=1)  # we get the next day
        return output


def get_default_date_data(start_date, end_date, default_value):
    start_date, end_date = validate_input_date_format(start_date, end_date)
    output = []
    if start_date and end_date and start_date <= end_date:
        days_range = abs((end_date-start_date).days) + 1
        for _ in range(days_range):
            str_date = date_to_str(start_date)
            output.append({'date': str_date, 'participants': default_value})
            start_date += timedelta(days=1)  # Iterate day by days
    return output
